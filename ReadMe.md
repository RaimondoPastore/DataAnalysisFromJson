Data Analysis From Json
---------------
******************************************************************
<b>Author: Raimondo Pastore</b>
******************************************************************
The application calculates interesting numbers from data sets (JSON files):

- most popular restaurants and dishes,
- top users for orders and spending,
- min. and max. delivery distances,
- how much money was spent for order (on average).
- data mining analysis for orders for a chosen customers or restaurant [Apache Spark].

The user is assisted by a simple gui.

**********************************************************************************
### Datasets

### restaurants.json

Contains a list of 75 restaurants located in Helsinki centre area. 
- All restaurants are open at the same time (every day, 10.00 - 20.00)
- *city_slug* is an ID for a city.*localized_city_name* is what needs to be displayed on the user interface
- *currency* uses ISO 4217 format
    - https://en.wikipedia.org/wiki/ISO_4217
- *timezone* uses tz database format
    - https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
```
  {
         "city_slug": "helsinki",
         "currency": "EUR",
         "description": "Aliquid porro aperiam facere. Quidem vitae ducimus quod minima at nesciunt sint provident. Debitis et distinctio commodi dolorum impedit iste.",
         "id": "772560444769",
         "localized_city_name": "Helsinki",
         "location": {
             "lat": 60.176042,
             "lon": 24.936967
         },
         "name": "Nasty Carnitas",
         "phone_number": "0721954213",
         "timezone": "Helsinki/Europe"
     }
```
### orders.json
Contains a list of orders made between 8.1.-14.1.2018. 

- *time_received* (UTC timestamp) is when the restaurant received the order.
- *time_completed* (UTC timestamp) is when the order was delivered to the customer
- *customer.location* is customer’s location on the map
- An order doesn’t contain currency data (*price*). Currency of a restaurant will be used (^ see restaurant data).
- An order can include many different (menu) items. A customer can also buy multiple pieces of the same item (*quantity*)
- *restaurant_id* refers to *id*-fields in *restaurants.json*
```
   {
          "customer": {
              "first_name": "Lotta",
              "id": "221272963361",
              "last_name": "Peltosaari",
              "location": {
                  "lat": 60.16158,
                  "lon": 24.941292
              }
          },
          "id": "242676101671",
          "items": [
              {
                  "id": "182392348383",
                  "name": "1. pesto mozzarella stick",
                  "price": 12.99,
                  "quantity": 1
              },
              {
                  "id": "251594033937",
                  "name": "2. zucchini tacos",
                  "price": 11.0,
                  "quantity": 2
              }
          ],
          "restaurant_id": "772560444769",
          "time_completed": 1515431521,
          "time_received": 1515429361
      }
```
***********************************************************************************************************

### Architecture

The application has a Three-tier architecture:

- Data access layer -> datasource package 
- Business layer -> controller package
- Presentation layer -> view package

The data mining part is realized with the help of Apache Spark. 
The application execute a Spark Session and then the task, finally close the Session.
It's not the best way possible to use Spark, but I have used only to show the potential of this framerwork.
I calculate the association rules and support of the items in all orders of a customer or a restaurant.
I realized i small library with the name lib/rp.dataming.spark.jar linked in the project.

************************************************************************************************************
### Run Instruction

Instruction to run the file wolt.jar

Open a terminal:

<b>java -jar wolt.jar</b>

The files orders.json and restaurant.json have to be in folder data that is in the same folder of wolt.jar


************************************************************************************************************