package rp.dataminig;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowth.FreqItemset;
import org.apache.spark.mllib.fpm.FPGrowthModel;
import org.apache.spark.sql.SparkSession;
import org.spark_project.guava.base.Joiner;

import rp.dataminig.spark.Rule;

public class DataMining {


	public static void main(String args[]) throws FileNotFoundException, IOException {
		
	       
		SparkConf conf = new SparkConf().setAppName("Raimondo Pastore - Spark - Frequenza in un giorno")
										.setMaster("local[*]");
        
        SparkSession spark = SparkSession.builder().master("local[*]").getOrCreate();
       
        spark.sparkContext().setLogLevel(Level.OFF.toString());
   	}
	
	public static List<Rule> sparkAnalysis(List<List> transactions, double minSupport, int numPartition) {
		
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
		SparkConf conf = new SparkConf().setAppName("Raimondo Pastore - Spark Analysis")
										.setMaster("local[*]");
		
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<List> transactionsRdd = sc.parallelize(transactions);
		
		List<Rule> rules = generateRules(transactionsRdd, minSupport, numPartition);
				
		sc.close();
		
		return rules;
	}
	
	public static  List<Rule>  generateRules (JavaRDD<List> transactions, double minSupport, int numPartition) {
	
		FPGrowth fpg = new FPGrowth().setMinSupport(minSupport)
									 .setNumPartitions(numPartition);
 
		FPGrowthModel<String> model = fpg.run(transactions);
		
		Long size = transactions.count();
		
		Function<FPGrowth.FreqItemset<String>, Rule> mapper = i -> new Rule(i.javaItems(), i.freq(), size);
		
		List<Rule> rules = model.freqItemsets()
								.toJavaRDD()
								.collect()
								.stream()
								.map(mapper)
								.sorted((s1,s2)-> (int)(s2.getFreq()-s1.getFreq()))
								.collect(Collectors.toList());	

		return rules;
	}
	

}
