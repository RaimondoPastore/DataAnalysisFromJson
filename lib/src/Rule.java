package rp.dataminig;

import java.util.List;

public class Rule {
	
	private List<String> rule;
	private Long freq;
	private Long support;
	
	
	public Rule(List<String> rule, Long freq, Long support) {
		
		this.rule = rule;
		this.freq = freq;
		this.support = support;
		
	}
	
	public Double getSupport() {
		
		return (double)freq/(double)support;
	}
	
	public Long getFreq() {
		
		return freq;
	}
	
	@Override
	public String toString() {
		
		return rule.toString() + " -> " + "Freq: [ " + freq + " ] Support: [ " + getSupport()+ " ]";
	}
	

}
