package wolt.utility;

import java.math.BigDecimal;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import json.model.*;

public class Logic{
	
	private List<Order> list ;
	
	
	public Logic (List list) {
		
		this.list = list;
	}
	
	
    public BigDecimal averangeBill(Integer digits) {
    	
        OptionalDouble averange = list.stream()
				  					  .mapToDouble(Order::getBill)
				  					  .average();
    	
    	return new BigDecimal(averange.orElse(-1)).setScale(digits, BigDecimal.ROUND_HALF_DOWN);
    }
    
    public List transaction(Function<Order,List> mapper, Predicate<Order> p) {
        
        List<List> transactions = list.stream()
				  					  .filter(p)
        							  .map(mapper)
        							  .collect(Collectors.toList());
		return transactions;
    }
    
    public Stream topCosturmersForSpending() {
    	
        BinaryOperator <Double> adderDecimal = (a,b) -> new BigDecimal(a+b).setScale(3, BigDecimal.ROUND_HALF_DOWN)
        																   .doubleValue();

        Collector<Order, ?, Map<String, Double>> collector = Collectors.toMap(o -> o.getCustomer().getSign(),
        																	  Order::getBill, 
        																	  adderDecimal,
        																	  LinkedHashMap::new);
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
    			   	
    	return collectAndSort(list.stream(), collector, comparator);
    	
    }
    
    public Stream topCosturmersForOrders() {
    	
    	Function<Order, String> f = s -> s.getCustomer().getSign();

        Collector<Order,?,Map<String,Long>>  collector = Collectors.groupingBy(f, Collectors.counting());
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
    	
    	return collectAndSort(list.stream(), collector, comparator);
    	
    }
    
    
    public Stream topRestaurantsForOrder() {
    	
    	Function<Order, String> f = Order::getRestaurantId;
    	
        Collector<Order,?,Map<String,Long>>  collector = Collectors.groupingBy(f, Collectors.counting());
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
    			
    	return collectAndSort(list.stream(), collector, comparator);
    	
    }
    
    public Stream topDish() {
    	
    	Function <String, String> f = o -> o;
    	
        Collector<String, ?, Map<String, Long>>  collector = Collectors.groupingBy(f, Collectors.counting());
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
        
    	Stream<String> s = list.stream().flatMap(Order::getItemIdStream);
   	
    	return collectAndSort(s,collector,comparator);
    }
    
   
    public <V> Stream<Map.Entry<String,V>> collectAndSort(Stream s, Collector<?,?,Map<String,V>> collector, Comparator<Map<String,V>> comparator) {
    	
    	return ((Map)s.collect(collector)).entrySet()
    									  .stream()
    									  .sorted(comparator);
    }
    
}
