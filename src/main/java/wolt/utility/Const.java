package wolt.utility;

/**
 * The {@link Const} interface contains the constants.
 * 
 * @author Raimondo Pastore
 *
 *
 */
public interface Const {
	
	public final static String AVERANGE_SPEND = "Averange Spending";
	public final static String TOP_COSTRUMERS_SPENDING = "Top 10 costumers for spending";
	public final static String TOP_COSTRUMERS_ORDERS = "Top 10 costumers for number of orders";
	public final static String TOP_RESTURANTS_ORDERS = "Top 10 restaurants for number of orders";
	public final static String TOP_DISHES = "Top 10 dishes";
	public final static String SPARK_ANALYSIS_RESTAURANT = "Spark Analysis -> Restaurant ";
	public final static String SPARK_ANALYSIS_CUSTOMERS = "Spark Analysis -> Customers";
	public static final String MAX_DISTANCE = "Max delivery distance";
	public static final String MIN_DISTANCE = "Min delivery distance";
	
	public final static String EXIT = "EXIT";
	
	
	public static String[] getChoises() {
		
		String [] choises = new String[10];
		
		choises[0] = AVERANGE_SPEND;
		choises[1] = TOP_COSTRUMERS_SPENDING;
		choises[2] = TOP_COSTRUMERS_ORDERS;
		choises[3] = TOP_RESTURANTS_ORDERS;
		choises[4] = TOP_DISHES;
		choises[5] = MAX_DISTANCE;
		choises[6] = MIN_DISTANCE;
		choises[7] = SPARK_ANALYSIS_CUSTOMERS;
		choises[8] = SPARK_ANALYSIS_RESTAURANT;
		choises[9] = EXIT;
		
		return choises;
		
	}
	
	
}
