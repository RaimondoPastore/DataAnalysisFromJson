package wolt.view;

import java.io.*;

import javax.swing.JOptionPane;
import wolt.controller.Controller;
import wolt.utility.Const;


/**
 * Hello world!
 *
 */
public class App 
{
	private final static String WOLT = "WOLT";
	private final static String jsonOrders = "data/orders.json";
	private final static String jsonRestaurants = "data/restaurants.json";
	private final static String titleSparkAnalysis = "Spark Analysis";
	private final static String messSparkClient = "Choose the Customer";
	private final static String messSparkRest = "Choose the Restaurant";
	private final static String ExceptionMessage = "The Json files are in the wrong path!!!\n\n"
													+ "Right path:\n\n"
			                                        + jsonOrders + "\n"
			                                        + jsonRestaurants + "\n";

	
    public static void main(String[] args) throws IOException{

	    try{
    	 
	        Controller c = new Controller(jsonOrders, jsonRestaurants);
		   
	        String[] restaurantNames = new String[0];
	        
	        String[] customersNames = new String[0];
	    
	        String result = new String();
	        
	        do {
		        String choise = guiInit();
		        
		        if(choise == null || choise == Const.EXIT) {break;}
		        
		        if (choise != Const.SPARK_ANALYSIS_RESTAURANT || choise != Const.SPARK_ANALYSIS_CUSTOMERS) {
		        	result = c.factory(choise);
		        }
		        
		        if(choise == Const.SPARK_ANALYSIS_RESTAURANT) {
		        	
		        	if(restaurantNames.length == 0){restaurantNames = c.getAllRestaurantNames();}

			        String nameRestaurant = guiSpark(restaurantNames, titleSparkAnalysis, messSparkRest);
			        if(nameRestaurant != null) {
			        	result = c.sparkAnalysisRestaurants(nameRestaurant);
			        }
		        }
		        
		        if(choise == Const.SPARK_ANALYSIS_CUSTOMERS) {
		        	
		        	if(customersNames.length == 0){customersNames = c.getAllCustomersNames();}

		        	String nameClient = guiSpark(customersNames, titleSparkAnalysis, messSparkClient);
		        	if(nameClient != null) {		        	
		        		result = c.sparkAnalysisClients(nameClient);
		        	}
		        }
		        
		        if(result!=null && !result.isEmpty()) {
		        	JOptionPane.showMessageDialog(null, result, WOLT, 1);
		        }
		        
	        }while(true);
	        
	        JOptionPane.showMessageDialog(null, "Goodbye", WOLT, 1);
	        
		 }catch (FileNotFoundException ex) {
			 
			 JOptionPane.showMessageDialog(null, ExceptionMessage, WOLT, 1);
			 ex.printStackTrace();
	    }
	      
    }

 
    public static String guiSpark(String[] choises, String title, String message) {
    	
        String input = (String) JOptionPane.showInputDialog(null, message,
            title, JOptionPane.QUESTION_MESSAGE, null,
            choises, // Array of choices
            choises[0]); // Initial choice
    	
        return input;
    }
    
    public static String guiInit() {
    	
    	String[] choices = Const.getChoises();

        String input = (String) JOptionPane.showInputDialog(null, "Chooce",
            WOLT, JOptionPane.QUESTION_MESSAGE, null, 
            choices, // Array of choices
            choices[0]); // Initial choice
    	
        return input;
    }
    
    
}
