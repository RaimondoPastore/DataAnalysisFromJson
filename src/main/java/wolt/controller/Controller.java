package wolt.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import wolt.datasource.JsonOrdersDatasource;
import wolt.datasource.AbstractOrdersDatasource;
import wolt.datasource.AbstractRestaurantsDatasource;
import wolt.datasource.JsonMapRestaurantsDatasource;
import wolt.utility.Const;
import json.model.*;
import rp.dataminig.spark.DataMining;

/**
 * The class {@link Controller} represents the business logic of the application. 
 * 
 * @author ghostray
 *
 */
public class Controller {
	
	private AbstractRestaurantsDatasource restaurants;
	private AbstractOrdersDatasource orders;
	
	private final Integer TOP_SIZE = 10;
	
	/**
	 * 
	 * Constructor of the class that initializes the data source of restaurants and orders.
	 * 
	 * @param jsonOrders - {@link String} that represents the path of json file

	 * 
	 * @param jsonOrders - {@link String} that represents the path of json file
	 * @param jsonRestaurants - {@link String} that represents the path of json file
	 * 
	 * @throws IOException if the path of the json files is not correct
	 */
	public Controller (String jsonOrders, String jsonRestaurants) throws IOException {
		
		
		this.restaurants = new JsonMapRestaurantsDatasource<String>(jsonRestaurants, r -> r.getId());
		
		this.orders = new JsonOrdersDatasource(jsonOrders);
		
	}
	
	/**
	 * Returns a string as a response to the presentation level.
	 * 
	 * @param type - a {@link String}
	 * @return a {@link String}
	 */
	public String factory(String type) {
		
		switch(type) {
				
			case Const.AVERANGE_SPEND: return showAverangeBill();
		 	
			case Const.TOP_COSTRUMERS_SPENDING: return showTop10CostumersForSpending();
			
			case Const.TOP_DISHES: return showTop10Dishes();
			
			case Const.TOP_COSTRUMERS_ORDERS: return showTop10CosturmersForOrders();
					
			case Const.TOP_RESTURANTS_ORDERS: return showTop10Restaurant();
			
			case Const.MAX_DISTANCE: return showMaxDistance();
			
			case Const.MIN_DISTANCE: return showMinDistance();
			
			default: return null;
		}
	}
		

	/**
	 * Returns the average spending of all orders.
	 * 
	 * @return a {@link String}
	 */
	public String showAverangeBill() {
		
		return  "The averange spending is: " + orders.averageSpending() + "€";
		
	}
	
	/**
	 * Returns the top 10 customers by spending.
	 * 
	 * @return a {@link String}
	 */
	public String showTop10CostumersForSpending() {
		
		Stream stream = orders.topCustomersForSpending().stream();
		
		Function mapper = s -> s.toString().concat("€\n");
		
		String result = convertToString(stream, mapper, TOP_SIZE);
									    
		return  "Top 10 Costumers by spending\n\n" + result;
		
	}
	
	/**
	 * Returns the top 10 customers by number of orders.
	 * 
	 * @return a {@link String}
	 */
	private String showTop10CosturmersForOrders() {
		
		Stream stream = orders.topCustormersForOrders().stream();
		
		Function mapper = s -> s.toString().concat("\n");
		
		String result = convertToString(stream, mapper, TOP_SIZE);
									    
		return  "Top 10 Costumers by number of orders\n\n" + result;
	}
	
	/**
	 * Returns the top 10 dishes considering the amount of orders on a dish.
	 * 
	 * @return a {@link String}
	 */
	public String showTop10Dishes() {
		
		Stream stream = orders.topDishes().stream();
		
		Function mapper = s -> s.toString().concat("\n");
		
		String result = convertToString(stream, mapper, TOP_SIZE);
		
		return  "Top 10 Dishes\n\n" + result;
		
	}
	
	/**
	 * Returns the top 10 restaurants by number of orders.
	 * 
	 * @return a {@link String}
	 */
	public String showTop10Restaurant() {
		
		Function<Map.Entry, String> reduce = e -> restaurants.getName(e.getKey().toString()) + "=" + e.getValue();
		
		Stream stream =  orders.topRestaurantIdForOrders().entrySet()
				   								 .stream()
				   								 .limit(TOP_SIZE)
				   								 .map(reduce);
		
		Function mapper = s -> s.toString().concat("\n");
		
		String result = convertToString(stream, mapper, TOP_SIZE);
		
		return  "Top 10 Restaurants\n\n" + result;
		
	}
	
	/**
	 * This method runs Spark Session and calculates the association rules 
	 * for all orders of a specific restaurant and returns the top 10 rules 
	 * with support. 
	 * 
	 * @param nameRestaurant - a {@link String} that represents the name of a restaurant
	 * 
	 * @return a {@link String}
	 */
	public String sparkAnalysisRestaurants(String nameRestaurant) {
		

		String idRestaurant = restaurants.getId(nameRestaurant);
		
		List<List> ordersDishes = orders.groupDishesInOrdersForRestaurant(idRestaurant);
		
		StringBuilder b = new StringBuilder();
		DataMining.sparkAnalysis(ordersDishes, 0, 10).stream()
			                                         .limit(TOP_SIZE)
			                                         .map(s -> s.toString().concat("\n"))
			                                         .forEach(b::append);
		
		String result = "Spark Analysis for : '" + nameRestaurant + "'\n\n";
		
		result += b.toString().isEmpty() ? "No Result" : b.toString();
		
		return result;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
	}
	
	/**
	 * This method runs Spark Session and calculates the association rules 
	 * for all orders of a specific client and returns the top 10 rules 
	 * with support.
	 * 
	 * @param nameRestaurant - a {@link String} that represents the name of a client.
	 * 
	 * @return a {@link String}
	 */
	public String sparkAnalysisClients(String nameClient) {
		
		List<List> ordersDishes = orders.groupDishesInOrdersForClient(nameClient);
	
		StringBuilder b = new StringBuilder();
		DataMining.sparkAnalysis(ordersDishes, 0.2, 10).stream()
			                                           .limit(10)
			                                           .map(s -> s.toString().concat("\n"))
			                                           .forEach(b::append);
		
		String result = "Spark Analysis for : '" + nameClient  + "'\n\n";
		
		result += b.toString().isEmpty() ? "No Result" : b.toString();
		
		return result;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
	}
	
	
	/**
	 * Return the maximum delivery distance.
	 * 
	 * @return A {@link String}
	 */
	public String showMaxDistance() {
		
		return "The maximum delivery distance is: " + maxDistance().intValue() + " m";
	}
	
	/**
	 * Return the minimum delivery.
	 * 
	 * @return A {@link String}
	 */
	public String showMinDistance() {
		
		return "The minimum delivery distance is: " + minDistance().intValue() + " m";
	}
	

	/**
	 * Returns an array that contains names of all restaurants.
	 * 
	 * @return a {@link String[]}
	 */
	public String[] getAllRestaurantNames() {

        
        return restaurants.allNames();
	}
	
	/**
	 * Returns an array that contains names of all customers.
	 * 
	 * @return a {@link String[]}
	 */
	public String[] getAllCustomersNames() {

        return orders.allCustomersName();
        		                   
	}
	
	private String convertToString(Stream stream, Function mapper, Integer size) {
		
		StringBuilder b = new StringBuilder();
		
		stream.limit(size)
			  .map(mapper)
			  .forEach(b::append);
		
		return b.toString();
		
	}
	
	private Double maxDistance() {
		
		ToDoubleFunction<Order> mapper = o -> restaurants.getLocation(o.getRestaurantId()).distance(o.getCustomer().getLocation());
		
		Double max = orders.allOrders()
								   .stream()
								   .mapToDouble(mapper)
								   .max()
								   .orElse(-1);
	
		return max;
 	}
	
	private Double minDistance() {
		
		ToDoubleFunction<Order> mapper = o -> restaurants.getLocation(o.getRestaurantId()).distance(o.getCustomer().getLocation());
		
		Double max = orders.allOrders()
						   .stream()
						   .mapToDouble(mapper)
						   .min()
						   .orElse(-1);
		
		
		return max;
 	}
}


