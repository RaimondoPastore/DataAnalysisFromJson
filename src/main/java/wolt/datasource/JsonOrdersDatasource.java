package wolt.datasource;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import json.load.LoadJson;
import json.model.Customer;
import json.model.Order;
import json.model.Restaurant;
import scala.annotation.meta.param;

/**
 * Concrete class of {@link AbstractOrdersDatasource} that represents a Json datasource.
 * 
 * @author Raimondo Pastore
 * 
 *
 */
public class JsonOrdersDatasource extends AbstractOrdersDatasource{
	
	private List<Order> orders;
	
	/**
	 * The constructor of the class that represents the datasource with a {@link List}.
	 * 
	 * @param jsonFile - a {@link String} that represents the path of the json file.
	 * @throws IOException if the {@link param} jsonFile is not a path of a json file
	 */
	public JsonOrdersDatasource(String jsonFile) throws IOException {
		
		orders = LoadJson.listFromJson(jsonFile, Order[].class);
        
	}

	/**
	 * Concrete method that returns the average spending of all orders.
	 * 
	 * @return a {@link BigDecimal} that represents the average
	 */
	@Override
	public BigDecimal averageSpending() {
		
        OptionalDouble averange = orders.stream()
				  						.mapToDouble(Order::getBill)
				  						.average();

        return new BigDecimal(averange.orElse(-1)).setScale(3, BigDecimal.ROUND_HALF_DOWN);
	}


	/**
	 *  Concrete method that returns a {@link List} contains the customers, sorted for spending.
	 *  
	 * @return a {@link List} that represents all the customers
	 */
	@Override
	public List topCustomersForSpending() {
	
        BinaryOperator <Double> adderDecimal = (a,b) -> new BigDecimal(a+b).setScale(3, BigDecimal.ROUND_HALF_DOWN)
				   .doubleValue();

        Collector<Order, ?, Map<String, Double>> collector = Collectors.toMap(o -> o.getCustomer().getSign(),
					  Order::getBill, 
					  adderDecimal,
					  LinkedHashMap::new);

        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
        

        return collectAndSort(orders.stream(), collector, comparator);
        		                   
	}

	/**
	 *  Concrete method that returns a {@link List} contains the customers sorted for number of orders.
	 *  
	 * @return a {@link List} that represents all the customers
	 */
	@Override
	public List topCustormersForOrders() {
		
    	Function<Order, String> f = o -> o.getCustomer().getSign();
    	
        Collector<Order,?,Map<String,Long>>  collector = Collectors.groupingBy(f, Collectors.counting());
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
    			
    	return collectAndSort(orders.stream(), collector, comparator);
	}

	/**
	 *  Abstract method that returns a {@link List} contains all the dishes, sorted for frequency.
	 *  
	 * @return a {@link List} that represents all the dishes
	 */
	@Override
	public List topDishes() {
		
    	Collector<String, ?, Map<String, Long>>  collector = Collectors.groupingBy(Function.identity(), Collectors.counting());
        
        Comparator comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
        
    	Stream<String> s = orders.stream().flatMap(Order::getItemNameStream);
   	
    	return collectAndSort(s,collector,comparator);
	}
	
	/**
	 *  Abstract method that returns a {@link List} contains all dishes for every orders of a specific restaurant.
	 *  
	 * @param idClient - a {@link String} that represents the id of a restaurant.
	 * 
	 * @return a {@link List<List>} every List contains the dishes of every orders.
	 */
	@Override
	public List<List> groupDishesInOrdersForRestaurant(String idRestaurant) {
		
        Function<Order, List> mapper = Order::getItemNameList;
        
        Predicate<Order> p = o -> o.getRestaurantId().equals(idRestaurant);
        
		return ordersList(mapper, p);
	}
 	
	/**
	 *  Abstract method that returns a {@link List} contains all dishes for every orders of a specific client.
	 *  
	 * @param nameClient - a {@link String} that represents the name of a client.
	 * 
	 * @return a {@link List<List>} every List contains the dishes of every orders.
	 */
 	@Override
	public List<List> groupDishesInOrdersForClient(String nameClient) {
		
        Function<Order, List> mapper = Order::getItemNameList;
        
        Predicate<Order> p = o -> o.getCustomer().getSign().equals(nameClient);
        
		return ordersList(mapper, p);
	}

	
	@Override
	public Map<String, Long> topRestaurantIdForOrders() {
		
    	Function<Order, String> f = Order::getRestaurantId;
    	
        Collector<Order,?,Map<String,Long>>  collector = Collectors.groupingBy(f, Collectors.counting());
        
        Comparator<Map.Entry<String, Long>> comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
        
    	Map<String, Long> map = orders.stream().collect(collector);
    	
    	map = map.entrySet().stream()
    						.sorted(comparator)
    						.collect(Collectors.toMap(Map.Entry::getKey,
    												  Map.Entry::getValue,
    												  (oldValue, newValue) -> oldValue,
    												  LinkedHashMap::new));
    	
    	return map; 
	}
	
	/**
	 *  Concrete method that returns a {@link List} contains all the customers.
	 *  
	 * @return a {@link List} that represents all the customers
	 */
	@Override
	public List<Customer> allCustomers() {
		
		return orders.stream()
					  .map(o->o.getCustomer())
					  .distinct()
					  .collect(Collectors.toList());
		
	}
	
	/**
	 *  Concrete method that returns a {@link String[]} contains all the customers names.
	 *  
	 * @return a {@link String[]} that represents all the customers name
	 */
	@Override
	public String[] allCustomersName() {

		
		return allCustomers().stream()
							 .map(Customer::getSign)
							 .sorted(String::compareTo)
							 .toArray(size -> new String[size]);
	
	}
	
	/**
	 *  Concrete method that returns a {@link List} contains all the orders.
	 *  
	 *  It's reference to a copy.
	 *  
	 * @return a {@link List} that represents all the orders
	 */
	@Override
	public List allOrders() {
		
		return orders.stream().collect(Collectors.toList());
	}
	
	
    private <V> List collectAndSort(Stream s, Collector<?,?,Map<String,V>> collector, Comparator<Map<String,V>> comparator) {
    	
    	return (List) ((Map)s.collect(collector)).entrySet()
    									  		 .stream()
    									  		 .sorted(comparator)
    									  		 .collect(Collectors.toList());
    }
    
    private List ordersList(Function<Order,List> mapper, Predicate<Order> p) {
        
        List<List> ordersList = orders.stream()
				  					  .filter(p)
        							  .map(mapper)
        							  .collect(Collectors.toList());
        
		return ordersList;
    }

}
