package wolt.datasource;

import json.model.Location;


/**
 * Abstract class that represents a datasource to load restaurants informations
 * from a different sources like Json file, Xml File or Database.
 * 
 * 
 * @author Raimondo Pastore
 *
 */
public abstract class AbstractRestaurantsDatasource {
	
	
	/**
	 * Abstract method that allows to return an array with the name of all restaurants from datasource.
	 *
	 * @return a {@link String} array 
	 */
	public abstract String[] allNames();
	
	/**
	 * Abstract method that allows to return the id of the restaurant from a given name.
	 * 
	 * @param name - {@link String} that represents the name of the restaurant
	 * 
	 * @return a {@link String} that represents the id of the restaurant from a given name.
	 */
	public abstract String getId(String name);
	
	/**
	 * Abstract method that allows to return the name of a restaurant from a given id.
	 * 
	 * @param id - {@link String} that represents the id of the restaurant
	 * 
	 * @return a {@link String} that represents the name of the restaurant from a given id.
	 */
	public abstract String getName(String id);
	
	
	/**
	 * Abstract method that allows to return the location of a restaurant from a given id.
	 * 
	 * @param id - {@link String} that represents the id of the restaurant
	 * 
	 * @return a {@link Location} that represents the location of the restaurant from a given id.
	 */
	public abstract Location getLocation(String id);
	
	

}
