package wolt.datasource;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import json.model.Customer;

/**
 * Abstract class that represents a Datasource to load informations
 * of orders from a different sources like Json file, Xml File or Database.
 * 
 * @author Raimondo Pastore
 *
 */
public abstract class AbstractOrdersDatasource {
	

	/**
	 * Abstract method that returns the average spending of all orders.
	 * 
	 * @return a {@link BigDecimal} that represents the average spending of all orders.
	 */
	public abstract BigDecimal averageSpending();
	
	/**
	 *  Abstract method that returns a {@link List} that contains all orders.
	 *  
	 * @return a {@link List} that represents all orders
	 */
	 
	public abstract List allOrders();
	
	/**
	 *  Abstract method that returns a {@link List} that contains all customers sorted by spending.
	 *  
	 * @return a {@link List} that represents all the customers sorted by spending.
	 */
	public abstract List topCustomersForSpending();
	
	/**
	 *  Abstract method that returns a {@link List} that contains all customers sorted by the amount of orders.
	 * 
	 *  
	 * @return a {@link List} that represents all the customers sorted by the amount of orders.
	 */
	public abstract List topCustormersForOrders();
	
	/**
	 *  Abstract method that returns a {@link List} that contains all dishes sorted by number of orders.
	 *  
	 * @return a {@link List} that represents all dishes sorted by number of orders.
	 */
	public abstract List topDishes();
	
	public abstract Map<String,Long> topRestaurantIdForOrders();
	
	/**
	 *  Abstract method that returns a {@link List} which contains information of each customers.
	 *  
	 * @return a {@link List} that represents information of each customers.
	 */
	public abstract List allCustomers();
	
	
	/**
	 *  Abstract method that returns a {@link String[]} which contains the name of each customer.
	 *  
	 * @return a {@link String[]} which contains the name of each customer.
	 */
	public abstract String[] allCustomersName();
	
	
	/**
	 *  Abstract method that returns a {@link List} which contains the dishes in all orders made by a specific client.
	 *  
	 * @param idClient - a {@link String} that represents the id of a client.
	 * 
	 * @return a {@link List<List>} each List contains the dishes of each order.
	 */
	 
	public abstract List<List> groupDishesInOrdersForClient(String nameClient);

	/**
	 *  Abstract method that returns a {@link List} which contains all dishes in all orders made in a specific restaurant.
	 *  
	 * @param idClient - a {@link String} that represents the id of a restaurant.
	 * 
	 * @return a {@link List<List>} every List contains dishes in each order.
	 */
	public abstract List<List> groupDishesInOrdersForRestaurant(String idRestaurant);

}