package wolt.datasource;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import json.load.LoadJson;
import json.model.Location;
import json.model.Restaurant;
import scala.annotation.meta.param;

/**
 * Concrete class of {@link AbstractRestaurantsDatasource} that represents a Json datasource like a {@link Map}.
 * 
 * @author Raimondo Pastore
 * 
 * @param <K> the <code>class</code> of the key of the map.
 *
 */
public class JsonMapRestaurantsDatasource<K> extends AbstractRestaurantsDatasource {

	private Map<K, Restaurant> restaurants;
 
	/**
	 * The constructor of the class that represents the datasource with a {@link Map}.
	 * 
	 * @param jsonFile - a {@link String} that represents the path of the json file.
	 * @param keyMapper - {@link Function} that map the key.
	 * @throws IOException if the {@link param} jsonFile is not a path of a json file
	 */
	public JsonMapRestaurantsDatasource(String jsonFile, Function<Restaurant, K> keyMapper) throws IOException {
		
		restaurants = LoadJson.mapFromJson(jsonFile, Restaurant[].class, keyMapper, Function.identity());
	}

	/**
	 *Concrete method that allows to return an array with the name of all restaurants from datasource.
	 *
	 * @return a {@link String} array 
	 */
	@Override
	public String[] allNames() {

		return restaurants.entrySet()
						  .stream()
						  .map(r -> r.getValue().getName())
						  .toArray(size -> new String[size]);

	}

	/**
	 *Concrete method that allows to return the id of the restaurant from a given name.
	 * 
	 * @param name - {@link String} that represents the name of the restaurant
	 * 
	 * @return a {@link String} that represents the id of the restaurant from a given name.
	 */
	@Override
	public String getId(String nameRestaurant) {

		return restaurants.entrySet()
				          .stream()
				          .filter(r -> r.getValue().getName().equals(nameRestaurant))
				          .findAny()
				          .get()
				          .getValue()
				          .getId();
	}

	/**
	 *Concrete method that allows to return the name of a restaurant from a given id.
	 * 
	 * @param id - {@link String} that represents the id of the restaurant
	 * 
	 * @return a {@link String} that represents the name of the restaurant from a given id.
	 */
	@Override
	public String getName(String id) {

		return restaurants.get(id).getName();
				

	}

	/**
	 *Concrete method that allows to return the location of a restaurant from a given id.
	 * 
	 * @param id - {@link String} that represents the id of the restaurant
	 * 
	 * @return a {@link Location} that represents the location of the restaurant from a given id.
	 */
	@Override
	public Location getLocation(String id) {
		
		return restaurants.get(id).getLocation();
	}

}
