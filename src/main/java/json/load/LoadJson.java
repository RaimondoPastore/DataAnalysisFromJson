package json.load;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import json.model.Order;
import json.model.Restaurant;

/**
 * The class {@link LoadJson} is used to load an array of object from a Json file.
 * 
 * 
 * @author Raimondo Pastore
 *
 */
public class LoadJson{
	
	/**
	 * It converts the array of object contained in json File, into a List of POJO of Class T .
	 * 
	 * @param file - {@link String} represents the path of the json file
	 * @param type - {@link Class}  it maps the json object in a POJO class 
	 * @return - {@link List} with all the object in the json file
	 * 
	 * @throws IOException if the path of json file is not correct.
	 */
	public static <T> List listFromJson(String file, Class<T[]> type) throws IOException  {
		
		List list = new ArrayList();
		
		try (FileReader fileReader = new FileReader(file)){
			
			BufferedReader reader = new BufferedReader(fileReader);
	        Gson gson = new GsonBuilder().create();
	        T[] array  =  (T[]) gson.fromJson(reader, type);
	        list = Arrays.asList(array);
        
		}
        
		return list;
		
	}
	
	/**
	 * It converts the array of object contained in json File, into a Map of POJO of Class V.
	 * 
	 * @param file - {@link String} represents the path of the json file
	 * @param type - {@link Class}  it maps the json object in a POJO class 
	 * @param keyMapper - a {@link Function} maps the key of the map
	 * @param valueMapper - a {@link Function} maps the value of the map
	 * @return - {@link Map} with all the object in the json file
	 * 
	 * @throws IOException if the path of json file is not correct.
	 */
	public static <K, V> Map<K,V> mapFromJson(String file, Class<V[]> type, Function<V,K> keyMapper, Function<V,V> valueMapper) throws IOException  {
		
		Map<K,V> map = null;
		
		try (FileReader fileReader = new FileReader(file)){		
			
			BufferedReader reader = new BufferedReader(fileReader);
	        Gson gson = new GsonBuilder().create();
	        
	        V[] array  =  (V[]) gson.fromJson(reader, type);
	        
	        map = (Map<K, V>) Arrays.asList(array).stream()
	        									  .collect((Collector<V, K, V>) Collectors.toMap(keyMapper, valueMapper));
        
		}
        
		return map;
			
	}
	
}
