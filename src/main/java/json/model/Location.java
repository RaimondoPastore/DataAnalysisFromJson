package json.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The class Location is a POJO. It represent a geo-position  with latitude and longitude.
 * 
 * @author Raimondo Pastore
 *
 */
public class Location {

	@SerializedName("lat")
	@Expose
	private Double lat;
	@SerializedName("lon")
	@Expose
	private Double lon;
	
	public Double getLat() {
		return lat;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLon() {
		return lon;
	}
	
	public void setLon(Double lon) {
		this.lon = lon;
	}
	
	/**
	 * Calculates the distance of this object with another {@link Location} object
	 * 
	 * @param l - {@link Location}
	 * @return a {@link Double} represents the distance in meters.
	 */
	public Double distance(Location l) {
		
		double distance;
		
		distance = Math.acos(Math.sin((this.lat)*Math.sin(l.lat)) + 
				(Math.cos(this.lon)*Math.cos(l.lon))*Math.cos(this.lon-l.lon));
		
		distance*=6360;
			
		
		return distance;
		
	}

}