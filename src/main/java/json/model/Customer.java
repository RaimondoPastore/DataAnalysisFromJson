package json.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import scala.annotation.meta.param;


/**
 * The class Customer is a POJO. It represents a customer with a first name, last name, id and location.
 * 
 * @author Raimondo Pastore
 */
public class Customer {
	
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("location")
	@Expose
	private Location location;
	
	/**
	 * Returns the first name of the customer
	 *
	 * @return a {@link String}
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets the first name of the customer
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Returns the id of the customer
	 *
	 * @return a {@link String}
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id of the customer
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Returns the last name of the customer
	 *
	 * @return a {@link String}
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets the last name of the customer
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Returns the coordinate of the customer
	 *
	 * @return a {@link Location}
	 */
	public Location getLocation() {
		return location;
	}
	
	/**
	 * Sets the position of the customer
	 *
	 * @param location - a {@link Location} instance
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	
	/**
	 * Returns the sign of the customer
	 *
	 * @return a {@link String}
	 */
	public String getSign() {
		return new StringBuilder().append(firstName)
								  .append(" ")
								  .append(lastName)
								  .toString();
	}
	
	/**
	 * Returns the sign and the id of the customer
	 *
	 * @return a {@link String}
	 */
	public String toString() {
		return new StringBuilder().append(getSign())
								  .append(":")
								  .append(id)
								  .toString();
	}
	
	/**
	 * Compares two instance of {@link Customer}. Two Customers are equals if they have the same id.
	 * 
	 * {@link param} obj - the reference object with which to compare.
	 * 
	 * @return <code>true</code> if obj has the same id of this object, <code>false</code> otherwise
	 */
	@Override
	public boolean equals(Object obj) {
				
		if(obj == null)
			return false;
		
		Customer customer = (Customer) obj;
			
		if(customer.getId().equals(this.getId())){
			return true;
		}
		else
			return false;
	}
	
	
	@Override
	public int hashCode() {
	    return id.hashCode();
	}

}