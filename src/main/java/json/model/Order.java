package json.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

@SerializedName("customer")
@Expose
private Customer customer;
@SerializedName("id")
@Expose
private String id;
@SerializedName("items")
@Expose
private List<Item> items = null;
@SerializedName("restaurant_id")
@Expose
private String restaurantId;
@SerializedName("time_completed")
@Expose
private Integer timeCompleted;
@SerializedName("time_received")
@Expose
private Integer timeReceived;

public Customer getCustomer() {
return customer;
}

public void setCustomer(Customer customer) {
this.customer = customer;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public List<Item> getItems() {
return items;
}

public void setItems(List<Item> items) {
this.items = items;
}

public String getRestaurantId() {
return restaurantId;
}

public void setRestaurantId(String restaurantId) {
this.restaurantId = restaurantId;
}

public Integer getTimeCompleted() {
return timeCompleted;
}

public void setTimeCompleted(Integer timeCompleted) {
this.timeCompleted = timeCompleted;
}

public Integer getTimeReceived() {
return timeReceived;
}

public void setTimeReceived(Integer timeReceived) {
this.timeReceived = timeReceived;
}

public String toString() {
	return new StringBuilder().append(id)
							  .append(customer)
							  .toString();
}

public Double getBill() {
	
	return items.stream()
				.mapToDouble(i-> i.getPrice()*i.getQuantity())
				.sum();
}

public Stream<String> getItemIdStream() {
	
	return getItems().stream()
					 .map(i -> i.getId());
}

public List<String> getItemIdList(){
	
	return  getItemIdStream().collect(Collectors.toList());
}

public Stream<String> getItemNameStream() {
	
	return getItems().stream()
					 .map(i -> i.getName());
}

public List<String> getItemNameList(){
	
	return  getItemNameStream().collect(Collectors.toList());
}
}