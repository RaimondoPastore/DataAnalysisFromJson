package json.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The class Restaurant is a POJO. It represent a Restaurant  with latitude and longitude 
 * with citySlug, currency, description, id, localizedCityName, location, name,  phoneNumber
 * and timezone.
 * 
 * @author Raimondo Pastore
 *
 */
public class Restaurant {

	@SerializedName("city_slug")
	@Expose
	private String citySlug;
	@SerializedName("currency")
	@Expose
	private String currency;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("localized_city_name")
	@Expose
	private String localizedCityName;
	@SerializedName("location")
	@Expose
	private Location location;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("phone_number")
	@Expose
	private String phoneNumber;
	@SerializedName("timezone")
	@Expose
	private String timezone;


	public Restaurant() {
	}


	public Restaurant(String citySlug, String currency, String description, String id, String localizedCityName, Location location, String name, String phoneNumber, String timezone) {
		super();
		this.citySlug = citySlug;
		this.currency = currency;
		this.description = description;
		this.id = id;
		this.localizedCityName = localizedCityName;
		this.location = location;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.timezone = timezone;
	}

	public String getCitySlug() {
		return citySlug;
	}
	
	public void setCitySlug(String citySlug) {
		this.citySlug = citySlug;
	}
	
	public Restaurant withCitySlug(String citySlug) {
		this.citySlug = citySlug;
		return this;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public Restaurant withCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Restaurant withDescription(String description) {
		this.description = description;
		return this;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Restaurant withId(String id) {
		this.id = id;
		return this;
	}
	
	public String getLocalizedCityName() {
		return localizedCityName;
	}
	
	public void setLocalizedCityName(String localizedCityName) {
		this.localizedCityName = localizedCityName;
	}
	
	public Restaurant withLocalizedCityName(String localizedCityName) {
		this.localizedCityName = localizedCityName;
		return this;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public Restaurant withLocation(Location location) {
		this.location = location;
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Restaurant withName(String name) {
		this.name = name;
		return this;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Restaurant withPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
		return this;
	}
	
	public String getTimezone() {
		return timezone;
	}
	
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Restaurant withTimezone(String timezone) {
		this.timezone = timezone;
	return this;
	}

}


